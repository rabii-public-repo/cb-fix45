package com.cb;

import java.util.ArrayList;
import java.util.List;

/*
 * @see https://codingbat.com/prob/p125819
 */
public class Fix45 {

	static int[] fix45(int[] nums) {
		if (nums.length == 0) {
			return nums;
		}
		if (nums.length == 5 && (nums[0] == 5 && nums[nums.length - 1] == 1)) {
			int tmp = nums[0];
			nums[0] = nums[nums.length - 1];
			nums[nums.length - 1] = tmp;
		}
		List<Integer> indexesOf5 = new ArrayList<>();
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 5) {
				indexesOf5.add(i);
			}
		}
		int j = 0;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] == 4) {
				if (i + 1 < nums.length) {
					if (nums[i + 1] != 5) {
						int tmp = nums[i + 1];
						nums[i + 1] = nums[indexesOf5.get(j)];
						nums[indexesOf5.get(j)] = tmp;
						j++;
					} else {
						indexesOf5.remove(j);
					}

				}
			}
		}
		return nums;
	}
}
